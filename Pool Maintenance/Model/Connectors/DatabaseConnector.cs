﻿using Microsoft.Extensions.Configuration;
using NLog;
using Pool_Maintenance.Model.Helpers;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Threading.Tasks;

namespace Pool_Maintenance.Model.Connectors
{
    class DatabaseConnector
    {
        private static NLog.Logger logger = NLog.LogManager.GetCurrentClassLogger();
        private string connString;
        private bool debug;

        public DatabaseConnector(bool debug)
        {
            var configurationBuilder = new ConfigurationBuilder()
                .SetBasePath(Environment.CurrentDirectory)
                .AddJsonFile("Config.json")
                .Build();

            connString = configurationBuilder["SQL"];
            this.debug = debug;
        }

        public int UpdateAgentInstalledPrograms(Agent agent)
        {
            int retVal = 0;
            if (debug)
            {
                logger.Log(LogLevel.Debug, $"      Adding Programs for {agent.agentName}");
                //Console.WriteLine($"      Adding programs for {agent.agentName}");
            }



            Parallel.ForEach(agent.programList, new ParallelOptions { MaxDegreeOfParallelism = 10 }, program => PushAgentInfo(program, agent.programCount));
            //PushAgentInfo(agent.programList);

            return retVal;
        }

        private int PushAgentInfo(Program program, int programCount)
        {
            int retVal = 0;
            //Parallel.ForEach(programs, new ParallelOptions { MaxDegreeOfParallelism = 1 }, (program) => {
                using (var conn = new SqlConnection(connString))
                {
                    using (var cmd = new SqlCommand("dbo.UpdateAgentInstalledPrograms", conn))
                    {
                        try
                        {
                            cmd.CommandType = System.Data.CommandType.StoredProcedure;
                            cmd.Parameters.Add(new SqlParameter("@AgentName", program.agentName));
                            cmd.Parameters.Add(new SqlParameter("@ProgramName", program.programName));
                            cmd.Parameters.Add(new SqlParameter("@Versions", program.version));
                            cmd.Parameters.Add(new SqlParameter("@InstallDate", program.installDate.Date));
                            cmd.Parameters.Add(new SqlParameter("@TotalProgramCount", programCount));
                            cmd.Parameters.Add(new SqlParameter("@LastCheckDate", DateTime.Today));
                            conn.Open();
                            cmd.ExecuteNonQuery();
                        }
                        catch (Exception ex)
                        {
                            if (debug)
                            {
                                logger.Log(LogLevel.Error, $"      Error Writing to Database: \n     {ex.Message}");
                                //Console.WriteLine($"    Error Writing to Database: \n  {ex.Message}");
                            }
                            retVal = 1;
                        }
                    }
                }
            






            return retVal;
        }
    }
}
