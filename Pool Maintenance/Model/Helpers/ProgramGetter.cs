﻿using Microsoft.Win32;
using NLog;
using Pool_Maintenance.Model.JSONObjects;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Management.Automation;
using System.Text;
using System.Threading.Tasks;

namespace Pool_Maintenance.Model.Helpers
{
    public class ProgramGetter
    {
        private static NLog.Logger logger = NLog.LogManager.GetCurrentClassLogger();
        private bool debug = false;
        List<string> agentsNotUpdated;
        public ProgramGetter(bool debug)
        {
            this.debug = debug;
            agentsNotUpdated = new List<string>();
        }
        /// <summary>
        /// Gets the list of installed programs on-demand through powershell, given a list of agents.
        /// This requires access to your domain. 
        /// </summary>
        /// <param name="agent">the agents to get the programs for</param>
        /// <returns>semi-sanitized List<string> with the program list for the agent</returns>
        public ConcurrentBag<Agent> GetInstalledProgramsConcurrent(Value[] agents)
        {
            ConcurrentBag<Agent> myBag = new ConcurrentBag<Agent>();
            //Console.WriteLine($"Using {Environment.ProcessorCount} processors");
            Parallel.ForEach(
                agents,
                agent => myBag.Add(new Agent(GetPrograms(agent.Name), agent.Name))
                );
            return myBag;
        }

        /// <summary>
        /// This is not good for large lists. Memory taken is > 1GB which is not acceptable. It's not even faster than Concurrent.
        /// </summary>
        /// <param name="agents"></param>
        //public void GetinstalledProgramsParallel(Value[] agents)
        //{
        //    ConcurrentBag<Agent> myBag = new ConcurrentBag<Agent>();
        //    var list = new List<Task>();

        //    foreach (var agent in agents)
        //    {
        //        var t = new Task(() =>
        //        {
        //            myBag.Add(new Agent(GetPrograms(agent.Name), agent.Name));
        //        });
        //        list.Add(t);
        //        t.Start();
        //    }
        //    Task.WaitAll(list.ToArray());
        //}

        /// <summary>
        /// This is not good for large lists. Memory taken is > 1GB which is not acceptable. It's not even faster than Concurrent.
        /// </summary>
        /// <param name="agents"></param>
        public void GetinstalledProgramsParallel(Value[] agents)
        {
            ConcurrentBag<Agent> myBag = new ConcurrentBag<Agent>();
            var list = new List<Task>();

            Parallel.ForEach(agents, agent => list.Add(Task.Factory.StartNew(() =>
            {
                myBag.Add(new Agent(GetPrograms(agent.Name), agent.Name));
            }))
            );

            //foreach (var agent in agents)
            //{
            //    var t = new Task(() =>
            //    {
            //        myBag.Add(new Agent(GetPrograms(agent.Name), agent.Name));
            //    });
            //    list.Add(t);
            //    t.Start();
            //}
            Task.WaitAll(list.ToArray());

        }

        public async Task<ConcurrentBag<Agent>> GetInstalledProgramsAsync(Value[] agents)
        {
            ConcurrentBag<Agent> myBag = new ConcurrentBag<Agent>();
            var list = new List<Task>();
            foreach (var agent in agents)
            {
                var t = new Task(() =>
                {
                    myBag.Add(new Agent(GetInstalledProgramsFromAgentAsync(agent.Name).Result, agent.Name));
                });
                list.Add(t);
                t.Start();
            }
            await Task.WhenAll(list.ToArray());
            return myBag;
        }
        private async Task<List<Program>> GetInstalledProgramsFromAgentAsync(string agent)
        {
            List<Program> installedPrograms = new List<Program>();
            await Task.Run(() =>
            {
                installedPrograms = GetPrograms(agent);
            });
            return installedPrograms;
        }

        /// <summary>
        /// Gets the list of installed programs for an agent.
        /// </summary>
        /// <param name="agent">The agent</param>
        /// <returns>List<Program> the list of programs.</returns>
        private List<Program> GetPrograms(string agent)
        {
            if(agent.ToLower().Contains("agent-"))
            {
                agent = agent.Substring(6, agent.Length - 6);
            }

            //if (agent.ToLower().Contains("#"))
            //{
            //    int index = agent.IndexOf('#');
            //    agent = agent.Substring(0, index);
            //    //Console.WriteLine(agent);
            //}

            List<Program> installedPrograms = new List<Program>();
            PowerShell PowerShellInstance = PowerShell.Create();

            var script = File.ReadAllText("Script.txt");
            PowerShellInstance.AddScript($"Invoke-Command -ComputerName {agent} -ScriptBlock {{{script}}}");

            Collection<PSObject> PSOutput = PowerShellInstance.Invoke();

            if (PowerShellInstance.Streams.Error.Count > 0)
            {
                agentsNotUpdated.Add(agent);
                if (debug)
                {
                    logger.Log(LogLevel.Warn, $"  Error fetching programs for {agent}:\n  {PowerShellInstance.Streams.Error[0].ToString()}");
                    //Console.WriteLine($"  Error fetching programs for {agent}:\n    {PowerShellInstance.Streams.Error[0].ToString()}");
                }
            }
            else
            {
                if (debug)
                {
                    logger.Log(LogLevel.Debug, $"  Got programs for {agent}");
                    //Console.WriteLine($"  Got programs for {agent}");
                }
                foreach (PSObject outputItem in PSOutput)
                {
                    if (outputItem != null)
                    {
                        DateTime installedDate = new DateTime(2001, 01, 01);
                        string agentName = outputItem.Members["PSComputerName"].Value.ToString();
                        string programName = outputItem.Members["ProgramName"].Value.ToString();
                        string version = outputItem.Members["Version"].Value != null ? outputItem.Members["Version"].Value.ToString() : "N/A";
                        StringBuilder installDate = new StringBuilder(outputItem.Members["InstallDate"].Value != null ? outputItem.Members["InstallDate"].Value.ToString() : "NA");

                        //Clean Install Dates.
                        if(!installDate.Equals("NA") && installDate.Length <= 8)
                        {
                            //There are some random install dates that come in as mm/dd/yyyy instead of yyyymmdd.
                            if (installDate.ToString().Contains('/'))
                            {
                                var arr = installDate.ToString().Split('/');
                                var month = arr[0];
                                var day = arr[1];
                                var year = arr[2];
                                installDate.Clear().Append(year + month + day);
                            }
                            //There are some install dates that come in as yyyymdd instead of yyyymmdd.
                            if (installDate.Length == 7)
                            {
                                var year = installDate.ToString().Substring(0, 4);
                                var month = installDate[4];
                                var day = installDate.ToString().Substring(5, 2);
                                installDate.Clear().Append(year + "0" + month + day);
                            }

                            //There are some install dates that come in as yyyymd instead of yyyymmdd.
                            if (installDate.Length == 6)
                            {
                                var year = installDate.ToString().Substring(0, 4);
                                var month = installDate[4];
                                var day = installDate.ToString().Substring(5, 1);
                                installDate.Clear().Append(year + "0" + month + "0" + day);
                            }

                            //Finally, convert installDate to date
                            installedDate = DateTime.ParseExact(installDate.ToString(), "yyyyMMdd", System.Globalization.CultureInfo.InvariantCulture);
                        }

                        installedPrograms.Add(new Program(agentName, programName, installedDate, version));
                    }
                }
            }
            return installedPrograms;
        }
    }
}
