﻿using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using NLog;
using Pool_Maintenance.Model.Helpers;
using Pool_Maintenance.Model.JSONObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading;
using System.Threading.Tasks;
/// <summary>
/// The connector class that talks to ADO for agent information.
/// </summary>
namespace Pool_Maintenance.Model.Connectors
{
    public class ADOConnector
    {
        private static NLog.Logger logger = NLog.LogManager.GetCurrentClassLogger();
        private int retryCount = 0;
        private bool debug = false;
        private AuthenticationHeaderValue msftPat;
        TimeSpan timeout;
        CancellationTokenSource cancelToken;
        
        public ADOConnector(bool debug)
        {
            var configurationBuilder = new ConfigurationBuilder()
                .SetBasePath(Environment.CurrentDirectory)
                .AddJsonFile("Config.json")
                .Build();

            this.debug = debug;
            timeout = TimeSpan.FromSeconds(10);
            cancelToken = new CancellationTokenSource();

            try
            {
                msftPat = new AuthenticationHeaderValue("Basic",
                    Convert.ToBase64String(System.Text.ASCIIEncoding.ASCII.GetBytes(
                                 string.Format("{0}:{1}", "", configurationBuilder["PAT"]))));
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
        }

        /// <summary>
        /// Gets data from the given URL from VSTS using REST API code.
        /// </summary>
        /// <param name="url">The URL to use</param>
        /// <returns>A string representing the JSON response from VSTS</returns>
        public async Task<string> GetVSTSData(string url)
        {
            string content = null;
            cancelToken.CancelAfter(timeout);
            //Console.WriteLine(url);
            using (HttpClient client = new HttpClient())
            {
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Authorization = msftPat;
                try
                {
                    var asyncResponse = client.GetAsync(url);
                    var response = await asyncResponse;
                    response.EnsureSuccessStatusCode();

                    content = response.Content.ReadAsStringAsync().GetAwaiter().GetResult();
                } catch (TaskCanceledException taskCancelled)
                {
                    if(taskCancelled.CancellationToken == cancelToken.Token)
                    {
                        logger.Log(LogLevel.Error, $"    HTTP Request failed: {url}\n   {taskCancelled.Message}.\n   Retrying in 30 seconds");
                        //Console.WriteLine($"    Error: HTTP request failed:\n{taskCancelled.Message} \nRetrying in 30 seconds");
                        Thread.Sleep(TimeSpan.FromSeconds(30));
                        if (retryCount == 0)
                        {
                            return await GetVSTSData(url);
                        }
                    }
                }
                catch (HttpRequestException requestEx)
                {
                    logger.Log(LogLevel.Error, $"    HTTP Request failed: {url}\n   {requestEx.Message}.\n   Retrying in 30 seconds");
                    //Console.WriteLine($"    Error: HTTP request failed:\n{requestEx.Message} \nRetrying in 30 seconds");
                    Thread.Sleep(TimeSpan.FromSeconds(30));
                    if (retryCount == 0)
                    {
                        return await GetVSTSData(url);
                    }
                }

            }
            retryCount = 0;
            return content;
        }

        ///<summary>Gets the id of a pool. Given a wildcard, gets all pool IDs matching the name.</summary>
        ///<param name="queueName">The pool name to get the ID for</param>
        ///<param name="wildcard">Whether or not to use contains or equals</param>
        ///<returns name="IEnumerable<int>">The pool ID's that match the pool name.</returns>
        private async Task<Dictionary<string, int>> GetPoolID(string poolName, bool wildcard)
        {
            if(debug)
            {
                logger.Log(LogLevel.Info, $"   Getting Pool ID's");
            }
            Dictionary<string, int> poolID = new Dictionary<string, int>();
            JSONQueueList queues = await GetPools("microsoft");
            foreach (var queue in queues.Value)
            {
                if(wildcard)
                {
                    if (queue.Name.ToLower().Contains(poolName.ToLower()))
                    {
                        poolID.Add(queue.Name, queue.ID);
                    }
                } else
                {
                    if (queue.Name.ToLower().Equals(poolName.ToLower()))
                    {
                        poolID.Add(queue.Name, queue.ID);
                    }
                }
                
            }

            return poolID;
        }

        /// <summary>
        /// Gets all pools in an instance.
        /// </summary>
        /// <param name="instance"></param>
        /// <returns></returns>
        public async Task<JSONQueueList> GetPools(string instance)
        {
            JSONQueueList response = null;

            string URI = "https://" + instance + ".visualstudio.com/_apis/distributedtask/pools?" + CommonProperties.apiVersion5_1Preview;

            var asyncResponse = GetVSTSData(URI);
            string JSONResponse = await asyncResponse;

            if (!JSONResponse.Contains("Bad"))
            {
                response = JsonConvert.DeserializeObject<JSONQueueList>(JSONResponse);
            }

            return response;
        }

        /// <summary>
        /// Gets the set of agents from the pool given the instance. Given a pool name, gets the ID for that pool.
        /// </summary>
        /// <param name="instance"></param>
        /// <param name="poolName"></param>
        /// <returns></returns>
        public async Task<IEnumerable<JSONAgentsList>> GetAllAgentsInPool(string instance, string poolName, bool wildcard)
        {

            JSONAgentsList response = null;
            List<JSONAgentsList> agentsList = new List<JSONAgentsList>();

            if (poolName != null)
            {
                //Get the list of PoolID's that correspond to the pool we've been given.
                Dictionary<string, int> poolIDs = GetPoolID(poolName, wildcard).Result;

                if (!(poolIDs.Count == 0) && poolIDs != null)
                {

                    foreach (string pool in poolIDs.Keys)
                    {
                        string URI;
                        if (pool.ToLower().Contains("codehub"))
                        {
                            URI = "https://mscodehub.visualstudio.com/_apis/distributedtask/pools/" + poolIDs[pool] + "/agents?includeCapabilities=true&" + CommonProperties.apiVersion5_1Preview;
                        }
                        else
                        {
                            URI = "https://microsoft.visualstudio.com/_apis/distributedtask/pools/" + poolIDs[pool] + "/agents?includeCapabilities=true&" + CommonProperties.apiVersion5_1Preview;
                        }

                        if(debug)
                        {
                            logger.Log(LogLevel.Info, $"   Getting agents for {pool}");
                            //Console.WriteLine("    Getting agents for {0}", pool);
                        }
                        
                        var asyncResponse = GetVSTSData(URI);
                        var JSONResponse = await asyncResponse;

                        if (!JSONResponse.Contains("Bad") && JSONResponse != null)
                        {
                            response = JsonConvert.DeserializeObject<JSONAgentsList>(JSONResponse);
                            if (response.Count > 0)
                            {
                                if (response.Value.ElementAt(0).Pool == null)
                                {
                                    var temp = response.Value.ElementAt(0);
                                    temp.Pool = new Pool();
                                    temp.Pool.Name = pool;
                                }
                                agentsList.Add(response);
                            }
                        } else
                        {
                            logger.Log(LogLevel.Error, $"    API returned error");
                            //Console.WriteLine("    API Returned Error.");
                        }
                    }
                }
            }
            return agentsList;
        }
    }
}
