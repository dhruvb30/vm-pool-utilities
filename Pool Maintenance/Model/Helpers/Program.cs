﻿using System;

namespace Pool_Maintenance.Model.Helpers
{
    public class Program
    {

        public string agentName { get; set; }
        public string programName { get; set; }
        public DateTime installDate { get; set; }
        public string version { get; set; }
        public Program(string agentName, string programName, DateTime installDate, string version)
        {
            this.agentName = agentName;
            this.programName = programName;
            this.installDate = installDate;
            this.version = version;
        }
    }
}
