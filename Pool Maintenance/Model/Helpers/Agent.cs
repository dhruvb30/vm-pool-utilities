﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Pool_Maintenance.Model.Helpers
{
    public class Agent
    {
        public List<Program> programList { get; set; }
        public string agentName { get; set; }
        public int programCount { get; set; }
        public Agent(List<Program> programList, string agentName)
        {
            this.programCount = programList.Count;
            this.agentName = agentName;
            this.programList = programList;
        }
    }
}
