﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Text;

namespace Pool_Maintenance.Model.JSONObjects
{
    public partial class JSONAgentsList
    {
        [JsonProperty("count")]
        public long Count { get; set; }

        [JsonProperty("value")]
        public Value[] Value { get; set; }
    }

    public partial class Value
    {
        [JsonProperty("authorization")]
        public Authorization Authorization { get; set; }

        [JsonProperty("createdOn")]
        public string CreatedOn { get; set; }

        [JsonProperty("enabled")]
        public bool Enabled { get; set; }

        [JsonProperty("id")]
        public int ID { get; set; }

        [JsonProperty("_links")]
        public Links Links { get; set; }

        [JsonProperty("maxParallelism")]
        public long MaxParallelism { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("osDescription")]
        public string OsDescription { get; set; }

        [JsonProperty("status")]
        public string Status { get; set; }

        [JsonProperty("version")]
        public string Version { get; set; }

        [JsonProperty("systemCapabilities")]
        [JsonExtensionData]
        public Dictionary<string, JToken> keyValuePair { get; set; }
    }


    public partial class Links
    {
        [JsonProperty("self")]
        public Self Self { get; set; }

        [JsonProperty("web")]
        public Self Web { get; set; }
    }

    public partial class Self
    {
        [JsonProperty("href")]
        public string Href { get; set; }
    }

    public partial class Authorization
    {
        [JsonProperty("clientId")]
        public string ClientId { get; set; }

        [JsonProperty("publicKey")]
        public PublicKey PublicKey { get; set; }
    }

    public partial class PublicKey
    {
        [JsonProperty("exponent")]
        public string Exponent { get; set; }

        [JsonProperty("modulus")]
        public string Modulus { get; set; }
    }

    public partial class JSONAgentsList
    {
        //public static JSONAgentsList FromJson(string json) => JsonConvert.DeserializeObject<JSONAgentsList>(json, Converter.Settings);
    }
}
