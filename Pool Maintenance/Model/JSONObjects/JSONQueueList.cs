﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace Pool_Maintenance.Model.JSONObjects
{
    public partial class JSONQueueList
    {
        [JsonProperty("count")]
        public long Count { get; set; }

        [JsonProperty("value")]
        public List<Value> Value { get; set; }
    }

    public partial class Value
    {
        [JsonProperty("groupScopeId")]
        public string GroupScopeId { get; set; }

        //[JsonProperty("id")]
        public long Id { get; set; }

        //[JsonProperty("name")]
        //public string Name { get; set; }

        [JsonProperty("pool")]
        public Pool Pool { get; set; }

        [JsonProperty("projectId")]
        public string ProjectId { get; set; }
    }

    public partial class Pool
    {
        [JsonProperty("id")]
        //public long PoolId { get; set; }

        //[JsonProperty("name")]
        public string Name { get; set; }

        //[JsonProperty("scope")]
        //public string Scope { get; set; }
    }

    public partial class JSONQueueList
    {
        //public static JSONQueueList FromJson(string json) => JsonConvert.DeserializeObject<JSONQueueList>(json, Converter.Settings);
    }
}
