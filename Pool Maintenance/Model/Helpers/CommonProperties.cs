﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Pool_Maintenance.Model.Helpers
{
    public class CommonProperties
    {
        //API variables
        public static string apiVersion1 = "api-version=1.0";

        public static string apiVersion2 = "api-version=2.3-preview.1";

        public static string apiVersion4 = "api-version=4.1";

        public static string apiVersion3Preview = "api-version=3.0-preview.1";

        public static string apiVersion5_1Preview = "api-version=5.1-preview.1";
    }
}
