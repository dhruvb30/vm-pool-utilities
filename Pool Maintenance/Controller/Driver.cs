﻿using NLog;
using Pool_Maintenance.Controller;
using Pool_Maintenance.Model.Connectors;
using Pool_Maintenance.Model.Helpers;
using Pool_Maintenance.Model.JSONObjects;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Diagnostics;

namespace Pool_Maintenance
{
    class Driver
    {
        static void Main(string[] args)
        {
            Logger logger = LogManager.GetLogger("*");
            string[] pools = new string[] { "Package ES *"};
            Stopwatch watch = new Stopwatch();
            ProgramUpdater updater = new ProgramUpdater(pools, true);
            var res = updater.PerformDatabaseUpdate("microsoft").Result;
            Console.WriteLine("Done.");
            Console.ReadLine();
        }
    }
}
