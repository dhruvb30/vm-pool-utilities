﻿using NLog;
using Pool_Maintenance.Model.Connectors;
using Pool_Maintenance.Model.Helpers;
using Pool_Maintenance.Model.JSONObjects;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Pool_Maintenance.Controller
{
    /// <summary>
    /// Contains the logic for updating the database.
    /// </summary>
    public class ProgramUpdater
    {
        private static NLog.Logger logger = NLog.LogManager.GetCurrentClassLogger();
        private string[] poolList;
        ProgramGetter programGetter;
        ADOConnector apiConnector;
        DatabaseConnector databaseConnector;
        private bool debug = false;
        public ProgramUpdater(string[] pools, bool debug)
        {
            poolList = pools;
            programGetter = new ProgramGetter(debug);
            apiConnector = new ADOConnector(debug);
            databaseConnector = new DatabaseConnector(debug);
            this.debug = debug;
        }

        public async Task<int> PerformDatabaseUpdate(string instance)
        {
            if(debug)
            {
                logger.Log(LogLevel.Debug, "Performing Database Update..");
            }
            int res = 0;
            for(int i = 0; i < poolList.Length; i++)
            {
                IEnumerable<JSONAgentsList> agentListRaw = await GetAgentsInPool(poolList[i], instance);
                ConcurrentBag<Agent> agentListClean = await GetInstalledPrograms(agentListRaw);
                res = UpdateDatabase(agentListClean);
                if(res != 0)
                {
                    break;
                }
            }
            return res;
        }
        /// <summary>
        /// Gets a list of agents in a pool
        /// </summary>
        /// <param name="pool">the agent pool</param>
        /// <param name="instance">the collection</param>
        /// <returns>the list of agents in that pool</returns>
        private async Task<IEnumerable<JSONAgentsList>> GetAgentsInPool(string pool, string instance)
        {
            
            IEnumerable<JSONAgentsList> agents;
            bool wildcard = false;
            if(pool.Contains("*"))
            {
                wildcard = true;
                pool = pool.Replace("*","");
                if (debug)
                {
                    logger.Log(LogLevel.Info, "  Wildcard found.");
                    logger.Log(LogLevel.Info, $"  Getting Agents containing {pool}");
                    //Console.WriteLine($"  Getting Agents containing {pool}");
                }
            }

            if (debug && !wildcard)
            {
                logger.Log(LogLevel.Debug, $"  Getting agents from {pool} ");
                //Console.WriteLine($"  Getting Agents from {pool}");
            }

            agents = await apiConnector.GetAllAgentsInPool(instance, pool, wildcard);
            return agents;
        }

        /// <summary>
        /// Gets installed programs from a list of agents.
        /// </summary>
        /// <param name="agents">the list of agents</param>
        /// <returns>a list of agents with installed program information.</returns>
        private async Task<ConcurrentBag<Agent>> GetInstalledPrograms(IEnumerable<JSONAgentsList> agents)
        {
            if (debug)
            {
                logger.Log(LogLevel.Debug, "  Getting Installed Programs..");
                //Console.WriteLine("  Getting Installed Programs..");
            }
            ConcurrentBag<Agent> agentDetails = new ConcurrentBag<Agent>();
            foreach (var agentList in agents)
            {
                var agts = await programGetter.GetInstalledProgramsAsync(agentList.Value);
                Parallel.ForEach(agts, a => agentDetails.Add(a));
            }
            return agentDetails;
        }
        /// <summary>
        /// Updates the list of installed programs in the database.
        /// </summary>
        /// <param name="agents">The list of agents with installed programs</param>
        private int UpdateDatabase(ConcurrentBag<Agent> agents)
        {
            if (debug)
            {
                logger.Log(LogLevel.Debug, "  Pushing Installed Programs");
                //Console.WriteLine("  Pushing Installed Programs..");
            }
            int retVal = 0;
            //We don't need to parallelize everything. For fast enough operations, there's no need for 
            //parallelism.
            foreach(var agent in agents)
            {
                retVal = databaseConnector.UpdateAgentInstalledPrograms(agent);
                if(retVal != 0)
                {
                    break;
                }
            }
            return retVal;
        }

    }
}
